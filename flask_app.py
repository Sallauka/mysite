
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, render_template, request

from controller import Controller, Gamestate

from person import Person



app = Flask(__name__)

gs=Gamestate()

@app.route('/', methods=["GET", "POST"])
def index():


    if request.method=="POST":

        Controller.process_request(request, gs)


        return render_template('index.html', STATE=gs)

    return render_template('index.html', STATE=gs)





@app.route('/my_table')
def my_table():

    all_people=Person.create_people(100)

    return render_template('table.html', ALL_PEOPLE=all_people)


