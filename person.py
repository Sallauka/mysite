import random


class Person:

    def __init__(self, age, height):

        self.age=age
        self.height=height

    @staticmethod
    def create_people(quantity):

        return[Person(age=random.randint(0,100), height=random.randint(100, 200)) for i in range(quantity)]
